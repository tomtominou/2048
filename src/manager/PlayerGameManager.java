package manager;

import core.game.adt.IGame;
import core.state.GameState;

/**
 * The game manager is the "controller" of the "model - view - controller" design pattern
 * It manages the game, the score, submits actions, checks if the game is over.
 * @author tomtom
 *
 * @param <Action>
 * @param <Game>
 */
public class PlayerGameManager<Action,Game extends IGame<Action, GameState>>{

	/**
	 * The current state of the game
	 */
	protected GameState s;
	/**
	 * The game played
	 */
	protected Game game;
	
	/**
	 * Constructor
	 * @param s
	 * @param game
	 */
	public PlayerGameManager(GameState s,Game game) {
		this.s = s;
		this.game = game;
		reset();
	}

	/*****************************/
	/** Playing functionalities **/
	/*****************************/
	
	/**
	 * Reset function : erases the board, sets the score to 0, and inserts two new random tiles
	 */
	public void reset(){
		for (int i = 0 ; i < s.getH()*s.getW() ; i++)
			s.setTileValue(i, 0);
		s.setScore(0);	
		
		game.natureAction(s);
		game.natureAction(s);
	}
	
	/**
	 * Function called when an action is triggered
	 * @param a
	 * @return true when we can keep playing, false is it is game over
	 */
	public boolean play(Action a){
		// If the player played successfully, i.e. if his action was possible, then play the nature
		if (playerAction(a))
			return natureAction();
		// Otherwise check for game over
		else return !s.isGameOver();
	}
	
	/**
	 * The player plays
	 * @param a
	 * @return true if the action was a possible/allowed move
	 */
	public boolean playerAction(Action a){
		return game.playerAction(a, s);
	}
	
	/**
	 * The action/opponent move
	 * @return true if an action was possible. 
	 */
	public boolean natureAction(){
		return game.natureAction(s);
	}
		
	/***********************/
	/** Getters / Setters **/
	/***********************/

	public GameState getS() {
		return s;
	}

	public void setS(GameState s) {
		this.s = s;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	/***************/
	/** Debugging **/
	/***************/
	public void display(){
		System.out.print("\n_________________________\n");
		for (int i = 0 ; i < s.getH() ; i++){
			System.out.println("");
			for (int j = 0 ; j < s.getW() ; j++)
				System.out.print("|"+ s.getTileValue(i,j) +"|");
		}
			
	}

}
