package manager;

import core.game.adt.IGame;
import core.state.GameState;
import ai.brain.adt.IAIBrain;
import util.Tools;

/**
 * A designer manager is an ai manager that allows the user to modify tile values, and the score, in order
 * to design a proper AI
 * @author tomtom
 *
 * @param <Action>
 * @param <Game>
 */
public class DesignerGameManager<Action,Game extends IGame<Action, GameState>> extends AIGameManager<Action,Game>{

	public DesignerGameManager(GameState s, Game game, IAIBrain<Action, GameState> brain) {
		super(s, game, brain);
	}
	
	/****************************************/
	/** AI designing/using functionalities **/
	/****************************************/
	
	/**
	 * Change the value of a certain tile
	 * @param index
	 * @param value
	 */
	public void forceTileValue(int index, int value){
		s.setTileValue(index, value);
		computeScore();
	}

	/**
	 * Change the value of a certain tile
	 * @param row
	 * @param col
	 * @param value
	 */
	public void forceTileValue(int row, int col, int value){
		s.setTileValue(row, col, value);
		computeScore();
	}
	
	/**
	 * Compute the maximum possible score attained considering the current state.
	 * A game state could be obtained with a score higher than the one computed here due
	 * to the random aspect of the nature action though.
	 */
	protected void computeScore() {
		int score = 0;
		for (int row = 0 ; row < s.getTiles().length ; row ++)
			for (int value : s.getTiles()[row]) score += Tools.scoreToReach(value);
		s.setScore(score);
	}
}
