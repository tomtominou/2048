package manager;

import core.game.adt.IGame;
import core.state.GameState;
import ai.brain.adt.IAIBrain;

/**
 * An AI manager is a game manager with an ai brain
 * @author tomtom
 *
 * @param <Action>
 * @param <Game>
 */
public class AIGameManager<Action, Game extends IGame<Action, GameState>> extends PlayerGameManager<Action,Game>{

	/**
	 * The brain
	 */
	private IAIBrain<Action, GameState> brain;

	/**
	 * Constructor
	 * @param state
	 * @param game
	 * @param brain
	 */
	public AIGameManager(GameState state, Game game, IAIBrain<Action, GameState> brain) {
		super(state,game);
		this.brain = brain ;
	}

	/***********/
	/*   AI    */
	/***********/
	
	/**
	 * Play the best move computed by the brain
	 * Return false is game is over
	 * @return true as long as we can keep playing, false if the game is over
	 */
	public boolean playBestMove(){
		
		// Find the best move :
		Action nextAction = brain.computeBestMove(super.s);
		
		if (nextAction == null) return false;

		// Play it :
		return super.play(nextAction);	
	}


}
