package util;

import java.util.Arrays;

public class Tools {

	public static Direction getOppositeOf(Direction direction){
		switch (direction) {
		case LEFT:
			return Direction.RIGHT;
		case RIGHT:
			return Direction.LEFT;
		case UP:
			return Direction.DOWN;
		case DOWN:
			return Direction.UP;
		}
		System.err.println("Unknown direction");
		return direction;
	}

	/**
	 * Checks if a number is a power of two
	 * @param value
	 * @return
	 */
	public static boolean isPowerOfTwo(int value) {
		double doubleValue = (double)value;
		while(doubleValue>1.)doubleValue/=2.0;
		if (doubleValue == 1.) return true;
		else return false;
	}

	/**
	 * Compte the sum
	 * @param value
	 * @return
	 */
	public static int scoreToReach(int value) {
		if (value == 0 || value == 2) return 0;
//		// Here we don't know if a 4 for generated, or a 2. So we compute the score with the same probabilities :
//		if (value == 4 && Math.random() > Grid.getP2()) return 0;
		// Otherwise :
		return value + 2*scoreToReach(value/2);
	}
	
	public static int[] quicksort(int[][] grid){
		// Init output :
		int[] output = new int[grid.length*grid[0].length];
		for (int i = 0 ; i < grid.length ; i++)
			for (int j = 0 ; j < grid[0].length ; j++)
				output[i*grid.length + j] = grid[i][j];
		Arrays.sort(output);
		return output;
	}
	
}
