package util;

public enum Direction {
	LEFT,
	UP,
	DOWN,
	RIGHT;
}
