package util;

import java.awt.Color;

public class TileColors {

	public static Color getBackground(int value){
		switch (value) {
		case 0:
			return new Color(204,192,179);
		case 2:
			return new Color(238,228,218);
		case 4:
			return new Color(237,224,200);
		case 8:
			return new Color(242,117,121);
		case 16:
			return new Color(245,149,99);
		case 32 :
			return new Color(246,124,95);
		case 64 :
			return new Color(246,94,59);
		case 128 :
			return new Color(237,207,114);
		case 256 :
			return new Color(237,204,97);
		case 516 :
			return new Color(237,200,80);
		case 1024 :
			return new Color(237,197,63);
		case 2048 :
			return new Color(237,194,46);
		case 4096 :
			return new Color(60,58,50);
		case 8192 :
			return new Color(28,140,77);
		case 16384 : 
			return new Color(42,60,219);
		case 32768 :
			return new Color(57,130,219);
		default :
			return new Color(141,32,219);
		}
	}

	public static Color getFont(int value){
		if (value == 2 || value == 4) return new Color(0,0,0);
		else return new Color(249,246,242);
	}
}
