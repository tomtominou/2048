package ai.brain.adtImpl;

import java.util.ArrayList;
import java.util.Iterator;

import core.game.adt.IGame;
import core.game.adtImpl.Game_2048;
import core.state.GameState;

import util.Direction;
import ai.brain.adt.IAIBrain;
import ai.eval.adt.IEvaluationFunction;

public class MinimaxResitenceBrain implements IAIBrain<Direction, GameState>{

	protected static int algoDepth = 5;
	protected static int minNumberOfZeroTiles = 6;
	protected IEvaluationFunction<GameState> evaluationFunction ;
	private IGame<Direction, GameState> game;

	public MinimaxResitenceBrain(IEvaluationFunction<GameState> evaluationFunction, IGame<Direction,GameState> game){
		this.evaluationFunction = evaluationFunction;
		this.game = game;
	}

	@Override
	public Direction computeBestMove(GameState state) {
		// Init the depth
		int step = algoDepth;

		// return the action that gets the minimum cost :
		double minCost = Double.MAX_VALUE;
		Direction bestMove = null;
		for (Direction direction : Direction.values()){
			// Copy game state
			GameState iteState = new GameState(state);
			// Simulate the action, without inserting any random tile
			// If nothing moved, consider that it is the worst possible action
			if (!game.playerAction(direction, iteState)) continue;
			// Compute its cost :
			double iteCost = computeExpectedDirectionCost(iteState,step,minCost);
			// Compare the cost :
			if (iteCost <= minCost){
				minCost = iteCost;
				bestMove = direction;
			}
		}
		return bestMove;
	}

	private double computeExpectedDirectionCost(GameState state, int step, double currentMinimumCost) {
		// if the we reached the depth max, the cost is given by the evaluation function
		if (step == 0) return evaluationFunction.evaluate(state);

		// Otherwise, we compute the expectation of the cost on all the possible outcomes
		// get the outcomes : the list of possible tile insertion :
		ArrayList<Integer> zeroIndexes = state.getZerosIndexes();
		// Initialize the expected value :
		double expectedValue = 0.;

		// If the number of zeros is small enough, explore them. Otherwise, just insert one at random :
		int nbOfZeros = zeroIndexes.size();
		if(nbOfZeros<minNumberOfZeroTiles){
			Iterator<Integer> ite = zeroIndexes.iterator();
			while(ite.hasNext() && expectedValue < currentMinimumCost){
				int index = ite.next();
				// Insert a 2 :
				state.setTileValue(index, 2);
				// increment the expected value by the cost of the player's next best move :
				expectedValue += Game_2048.P2 * computePlayerBestActionCost(state, step-1) / (double)nbOfZeros;

//				// Insert a 4 :
//				state.setTileValue(index, 4);
//				// increment the expected value by the cost of the player's next best move :
//				expectedValue += (1.-Game_2048.P2) * computePlayerBestActionCost(state, step-1) / (double)nbOfZeros;

				// restore the state :
				state.setTileValue(index, 0);
			}
		}
		else{
			// otherwise, just do one random nature action :
			boolean gameOver = !game.natureAction(state);
			// And compute its cost
			if (!gameOver) expectedValue = computePlayerBestActionCost(state, step-1);
			// If the game is over, return the almost-worst value : the worst value is for impossible moves
			else expectedValue = Double.MAX_VALUE/2.;
		}
		return expectedValue;
	}

	private double computePlayerBestActionCost(GameState state, int step){
		// For every possible action, compute the expected cost :
		// Init the cost at the worst possible value : if the action is impossible, is it not considered.
		double minCost = Double.MAX_VALUE;

		for (Direction direction : Direction.values()){
			// Copy the state, in order to avoid modifying an object used by another function
			GameState iteState = new GameState(state);
			// Simulate the action, without inserting any random tile
			// If nothing moved, consider that it is the worst possible action
			if (!game.playerAction(direction, iteState)) continue;
			// Otherwise let the game go on, and evaluate the next moves : 
			double iteCost = computeExpectedDirectionCost(iteState,step,minCost);
			// Compare the cost :
			if (iteCost <= minCost) minCost = iteCost;
		}
		return minCost;
	}
}
