package ai.brain.adt;

public interface IAIBrain<Action,State> {

	public Action computeBestMove(State state);

}
