package ai.eval.adtImpl;

import core.state.GameState;
import ai.eval.adt.IEvaluationFunction;

public class ScoreFunction implements IEvaluationFunction<GameState>{

	@Override
	public double evaluate(GameState t) {
		return 100./(double)t.getScore();
	}
}
