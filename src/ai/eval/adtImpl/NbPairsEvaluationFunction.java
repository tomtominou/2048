package ai.eval.adtImpl;

import core.state.GameState;
import util.Tools;
import ai.eval.adt.IEvaluationFunction;

public class NbPairsEvaluationFunction implements IEvaluationFunction<GameState> {

	@Override
	public double evaluate(GameState t) {
		// Sort the array
		int[] values = Tools.quicksort(t.getTiles());
		int n = values.length;
		
		// Compare the score to the minimum score required to get the current tile values :
		// score will be minimal if only one tile of each value exists.
		// The grade is equal to the sum of the values of the unnecessary tiles :
		int cost = 0;
		for (int i = 0 ; i < n-1 ; i++){
			if(values[i] == values[i+1] && values[i] != 0) 
				cost ++;
		}

		return 100.*((double)((double)cost/16.) + 1./(1.+ (double)t.getScore()));
	}

}
