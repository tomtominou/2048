package ai.eval.adtImpl;

import core.state.GameState;
import util.Tools;

import ai.eval.adt.IEvaluationFunction;

/**
 * the grade is small when the score is the minimum possible score given the maximum value of a tile in the grid
 * @author tomtom
 *
 */
public class MinScoreEvaluationFunction implements IEvaluationFunction<GameState> {

	@Override
	public double evaluate(GameState t) {
		// Sort the array
		int[] values = Tools.quicksort(t.getTiles());
		int n = values.length;
		
		// Compare the score to the minimum score required to get the current tile values :
		// score will be minimal if only one tile of each value exists.
		// The grade is equal to the sum of the values of the unnecessary tiles :
		int cost = 0;
		int theoreticScore = 0;
		for (int i = 0 ; i < n-1 ; i++){
			theoreticScore += Tools.scoreToReach(values[i]);
			if(values[i] != values[i+1]) 
				cost += Tools.scoreToReach(values[i]);
		}
			
		theoreticScore += Tools.scoreToReach(values[n-1]);
		if (values[n-2] != values[n-1]) cost+= Tools.scoreToReach(values[n-1]);
		
		return (double)(theoreticScore - cost)/(double)t.getScore();
	}
}
