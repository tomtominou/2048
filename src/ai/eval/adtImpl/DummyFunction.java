package ai.eval.adtImpl;

import core.state.GameState;
import ai.eval.adt.IEvaluationFunction;

public class DummyFunction implements IEvaluationFunction<GameState> {

	@Override
	public double evaluate(GameState t) {
		return Math.random();
	}
}
