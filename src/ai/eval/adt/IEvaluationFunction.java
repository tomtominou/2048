package ai.eval.adt;

/**
 * Interface to create evaluation functions
 * @author tomtom
 *
 * @param <T>
 */
public interface IEvaluationFunction<T> {

	/**
	 * Function that gives a grade to a T
	 * @param t
	 * @return grade
	 */
	public double evaluate(T t);
}
