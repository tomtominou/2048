package exec;

import ai.brain.adt.IAIBrain;
import ai.brain.adtImpl.MinimaxResitenceBrain;
import ai.eval.adt.IEvaluationFunction;
import ai.eval.adtImpl.ScoreFunction;
import manager.DesignerGameManager;
import util.Direction;
import view.DesignerView;
import controller.DesignerController;
import core.game.adtImpl.Game_2048;
import core.state.GameState;

public class AIDesigning {

	public static void main(String[] args) {
		Game_2048 game = new Game_2048();
		GameState state = new GameState(4);
		IEvaluationFunction<GameState> function = new ScoreFunction();
		IAIBrain<Direction, GameState> brain = new MinimaxResitenceBrain(function, game);
		DesignerGameManager<Direction, Game_2048> manager = new DesignerGameManager<Direction, Game_2048>(state, game,brain);
		DesignerView<Game_2048> view = new DesignerView<Game_2048>(manager,function);
		DesignerController<Game_2048> controller = new DesignerController<Game_2048>(manager,view);
		view.setListener(controller);
	}

}
