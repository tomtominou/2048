package exec;

import controller.GameController;
import core.game.adtImpl.Game_2048;
import core.state.GameState;
import util.Direction;
import view.PlayerView;
import manager.PlayerGameManager;

public class Play {

	public static void main(String[] args){
		PlayerGameManager<Direction, Game_2048> manager = new PlayerGameManager<Direction, Game_2048>(new GameState(4), new Game_2048());
		PlayerView<Game_2048> view = new PlayerView<Game_2048>(manager);
		GameController<Game_2048> controller = new GameController<Game_2048>(manager,view);
		view.setListener(controller);
	}
}
