package exec;

import manager.AIGameManager;
import util.Direction;
import view.AIView;
import ai.brain.adt.IAIBrain;
import ai.brain.adtImpl.MinimaxResitenceBrain;
import ai.eval.adt.IEvaluationFunction;
import ai.eval.adtImpl.ScoreFunction;
import controller.AIController;
import core.game.adtImpl.Game_2048;
import core.state.GameState;

public class AIUsing {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Game_2048 game = new Game_2048();
		GameState state = new GameState(4);
		IEvaluationFunction<GameState> function = new ScoreFunction();
		IAIBrain<Direction, GameState> brain = new MinimaxResitenceBrain(function, game);
		AIGameManager<Direction, Game_2048> manager = new AIGameManager<Direction, Game_2048>(state, game,brain);
		AIView<Game_2048> view = new AIView<Game_2048>(manager);
		AIController<Game_2048> controller = new AIController<Game_2048>(manager,view);
		view.setListener(controller);
	}

}
