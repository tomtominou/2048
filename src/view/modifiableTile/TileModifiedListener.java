package view.modifiableTile;


public interface TileModifiedListener {

	public void tileModified(TileModifiedEvent e);
	
}
