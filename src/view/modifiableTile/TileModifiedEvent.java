package view.modifiableTile;

public class TileModifiedEvent{

	private int index;
	private int value;
	public TileModifiedEvent(int index, int value) {
		super();
		this.index = index;
		this.value = value;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	
	
}
