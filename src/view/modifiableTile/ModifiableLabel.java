package view.modifiableTile;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import core.game.adt.IGame;
import core.state.GameState;

import util.Direction;
import view.PlayerView;

public class ModifiableLabel<Game extends IGame<Direction, GameState>> extends JLabel implements MouseListener,KeyListener{

	private static final long serialVersionUID = 1L;
	private int index;
	private PlayerView<Game> view;
	private int newValueBuffer = 0;
	private List<TileModifiedListener> listeners = new ArrayList<TileModifiedListener>();

	public ModifiableLabel(int index,PlayerView<Game> view){
		this.index = index;
		this.addMouseListener(this);
		this.addKeyListener(this);
		this.view = view;
		setFocusable(true);
	}

	public void addListener(TileModifiedListener listener){
		listeners.add(listener);
	}
	
	/**
	 * When clicked, the label takes the window focus
	 */
	@SuppressWarnings("unchecked")
	private void takeFocus(){
		// Ask everyone else to release the focus :
		for (JLabel label : view.getTileLabels())
			((ModifiableLabel<Game>)label).releaseFocus();
		// request the focus :
		requestFocusInWindow();
		// set a red border, for the user to see :
		setBorder(BorderFactory.createLineBorder(Color.RED));
		// And reset the value of the buffer :
		newValueBuffer = 0;
	}
	
	/**
	 * Release the focus, when another label is marked, 
	 */
	public void releaseFocus(){
		// Give the focus back to the window :
		view.getFrame().requestFocus();
		// Set the border back to black :
		setBorder(/*BorderFactory.createLineBorder(Color.BLACK)*/null);
	}

	/**
	 * When we click on the label, we tell that we want to change its value
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		takeFocus();
	}

	@Override
	public void mouseEntered(MouseEvent e) {		
	}

	@Override
	public void mouseExited(MouseEvent e) {		
	}

	@Override
	public void mousePressed(MouseEvent e) {		
	}

	@Override
	public void mouseReleased(MouseEvent e) {		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER){
			releaseFocus();
			for (TileModifiedListener listener : listeners) listener.tileModified(new TileModifiedEvent(index, newValueBuffer));
			newValueBuffer = 0;		
		}
		else {
			char typed = e.getKeyChar();
			if (java.lang.Character.isDigit(typed)){
				newValueBuffer = 10*newValueBuffer + java.lang.Character.getNumericValue(typed);
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}

}
