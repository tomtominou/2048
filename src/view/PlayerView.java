package view;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.GameController;

import core.game.adt.IGame;
import core.state.GameState;

import util.Direction;
import util.TileColors;

import manager.PlayerGameManager;

public class PlayerView<Game extends IGame<Direction, GameState>>{

	/**
	 * game dimensions
	 */
	protected int h,w;
	/**
	 * the frame
	 */
	protected JFrame frame = new JFrame("2048");
	/**
	 * the labels
	 */
	protected List<JLabel> tileLabels;
	protected JLabel scoreLabel = new JLabel("0");
	/**
	 * The panels
	 */
	protected JPanel infoPanel;
	protected JPanel mainPanel;
	protected JPanel buttonPanels;
	protected JPanel labelsPanel;
	/**
	 * Reset button
	 */
	protected JButton resetButton;
	/**
	 * The colors used
	 */
	protected TileColors color = new TileColors();
	
	/**
	 * Constructor
	 * @param gameManager
	 * @param keyboardListenner
	 */
	public PlayerView(PlayerGameManager<Direction, Game> gameManager){
		this.h = gameManager.getS().getH();
		this.w = gameManager.getS().getW();
		
		// The main panel :
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout(50,10));
		mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// The panel for the other info (like the score) :
		infoPanel = new JPanel();
		mainPanel.add(infoPanel,BorderLayout.NORTH);
		infoPanel.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = .5;
		constraints.gridwidth = 1;
		scoreLabel.setBorder(BorderFactory.createTitledBorder("score"));
		infoPanel.add(scoreLabel,constraints);
		
		//The panel for the labels :
		labelsPanel = new JPanel();
		labelsPanel.setBackground(new Color(187,173,160));
		labelsPanel.setOpaque(true);
		labelsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		mainPanel.add(labelsPanel,BorderLayout.CENTER);
		labelsPanel.setLayout(new GridLayout(h,w,10,10));
		
		// create the labels :
		tileLabels = new ArrayList<JLabel>();
		for (int i = 0 ; i < h*w ; i++){
			JLabel label = new JLabel();
			label.setHorizontalAlignment(JLabel.CENTER);
			label.setVerticalAlignment(JLabel.CENTER);
			label.setFont (label.getFont ().deriveFont (30.0f));
			labelsPanel.add(label);
			tileLabels.add(label);
		}
		
		// Button panel
		buttonPanels = new JPanel();
		resetButton = new JButton("reset");
		buttonPanels.add(resetButton);
		mainPanel.add(buttonPanels,BorderLayout.SOUTH);
		
		// Finalize
		labelsPanel.setVisible(true);
		frame.setContentPane(mainPanel);
		frame.setVisible(true);
		frame.pack();
		frame.setSize(500, 500);
		frame.setFocusable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // DISPOSE_ON_CLOSE ?
	}

	public void display(GameState state) {
		scoreLabel.setText(new Integer(state.getScore()).toString());
		for (int i = 0 ; i < h*w ; i++) {
			JLabel label = tileLabels.get(i);
			Integer value = new Integer(state.getTileValue(i));
			label.setText(value ==0 ? "" : value.toString());
			label.setForeground(TileColors.getFont(value));
			label.setBackground(TileColors.getBackground(value));
			label.setOpaque(true);
		}
	}
	
	/*** Getter/Setter ***/
	public void setListener(GameController<Game> controller){
		resetButton.addActionListener(controller);
		frame.addKeyListener(controller);
	}
	public JButton getResetButton(){
		return resetButton;
	}
	public JFrame getFrame(){
		return frame;
	}
	public List<JLabel> getTileLabels() {
		return tileLabels;
	}
}
