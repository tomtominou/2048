package view;

import java.awt.GridBagConstraints;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import ai.eval.adt.IEvaluationFunction;

import manager.DesignerGameManager;
import controller.DesignerController;
import util.Direction;
import view.modifiableTile.ModifiableLabel;
import core.game.adt.IGame;
import core.state.GameState;

public class DesignerView<Game extends IGame<Direction,GameState>> extends AIView<Game>{

	private JLabel evaluationLabel;
	private IEvaluationFunction<GameState> evaluation;
	
	public DesignerView(DesignerGameManager<Direction, Game> manager,
			IEvaluationFunction<GameState> evaluation) {
		super(manager);
		this.evaluation = evaluation;
		
		//Modify the panel for the labels :
		labelsPanel.removeAll();
		tileLabels = new ArrayList<JLabel>();

		// create the labels :
		for (int i = 0 ; i < h*w ; i++){
			ModifiableLabel<Game> label = new ModifiableLabel<Game>(i,this);
			label.setHorizontalAlignment(JLabel.CENTER);
			label.setVerticalAlignment(JLabel.CENTER);
			label.setFont (label.getFont ().deriveFont (30.0f));
			labelsPanel.add(label);
			tileLabels.add(label);
		}
		
		// Add the score to the north panel :
		evaluationLabel = new JLabel("0");
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = .5;
		constraints.gridwidth = 1;
		evaluationLabel.setBorder(BorderFactory.createTitledBorder("evaluation"));
		infoPanel.add(evaluationLabel,constraints);
	}
	
	@Override
	public void display(GameState state){
		super.display(state);
		displayEvaluation(evaluation.evaluate(state));
	}
	
	private void displayEvaluation(double evaluation){
		DecimalFormat f = new DecimalFormat();
		f.setMaximumFractionDigits(2);
		evaluationLabel.setText(f.format(evaluation));
	}
	
	@SuppressWarnings("unchecked")
	public void setListener(DesignerController<Game> controller){
		super.setListener(controller);
		for (JLabel label : tileLabels) ((ModifiableLabel<Game>)label).addListener(controller);
	}

}
