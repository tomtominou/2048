package view;

import javax.swing.JButton;

import manager.AIGameManager;
import controller.AIController;
import util.Direction;
import core.game.adt.IGame;
import core.state.GameState;

public class AIView<Game extends IGame<Direction,GameState>> extends PlayerView<Game>{

	protected JButton autorunButton;
	protected JButton nextButton;
	
	/**
	 * Constructor
	 * @param gameManager
	 * @param controller
	 * @param evaluation
	 */
	public AIView(AIGameManager<Direction, Game> manager) {
		super(manager);
		
		// The new buttons :
		autorunButton = new JButton("autorun : start");
		super.buttonPanels.add(autorunButton);
		
		nextButton = new JButton("next move");
		super.buttonPanels.add(nextButton);
	}

	public JButton getAutorunButton() {
		return autorunButton;
	}

	public JButton getNextButton() {
		return nextButton;
	}
	
	public void setListener(AIController<Game> controller){
		super.setListener(controller);
		nextButton.addActionListener(controller);
		autorunButton.addActionListener(controller);
	}
}
