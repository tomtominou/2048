package core.state;

import java.util.ArrayList;

/**
 * The current state of the game
 * @author tomtom
 *
 */
public class GameState {

	private int[][] tileValues;
	private int score = 0;
	private int h,w;

	/**
	 * Constructors
	 * @param n
	 */
	/**
	 * square nxn
	 * @param n
	 */
	public GameState(int n){
		this(n,n);
	}
	/**
	 * rectangle hxw
	 * @param h
	 * @param w
	 */
	public GameState(int h, int w){
		this.h = h;
		this.w = w;
		tileValues = new int[h][w];
		for (int i = 0 ; i < h ; i++)
			for (int j = 0 ; j < w ; j++){
				tileValues[i][j] = 0;
			}
	}
	/**
	 * copy
	 * @param other
	 */
	public GameState(GameState other){
		this.w = other.w;
		this.h = other.h;
		this.tileValues = new int[w][h];
		for (int i = 0 ; i < h*w ; i++) this.setTileValue(i, other.getTileValue(i));
	}

	/**
	 * Increment the score
	 * @param increment
	 */
	public void incrementScore(int increment){
		score+=increment;
	}

	/**
	 * Sets the value of a certain tile
	 * @param index
	 * @param value
	 */
	public void setTileValue(int row, int col, int value){
		tileValues[row][col] = value;
	}
	public void setTileValue(int index, int value){
		tileValues[(index-index%w)/w][index%w] = value;
	}

	public int getTileValue(int row, int col){
		return tileValues[row][col];
	}
	public int getTileValue(int index){
		return tileValues[(index-index%w)/w][index%w];
	}

	/**
	 * Get an array of the indexes of the 0 valued tiles
	 * @return
	 */
	public ArrayList<Integer> getZerosIndexes(){
		ArrayList<Integer> output = new ArrayList<Integer>();
		for (int i = 0 ; i < h*w ; i++)
			if (getTileValue(i)==0) output.add(i);
		return output;
	}

	public void doubleTileValue(int row, int col) {
		setTileValue(row, col, 2*getTileValue(row, col));
	}

	/**
	 * Returns true if the game is over !
	 * @return
	 */
	public boolean isGameOver(){
		// first lines/columns
		for (int i = 0 ; i < h-1 ; i ++)
			for (int j = 0 ; j < w-1 ; j ++){
				int value = tileValues[i][j];
				if ( value == 0 || tileValues[i][j+1] == value|| tileValues[i+1][j] == value)
					return false;
			}
		// last line : 
		for (int j = 0 ; j < w-1 ; j++ ){
			int value = tileValues[h-1][j];
			if (value == 0 || tileValues[h-1][j+1] == value) return false;
		}
		// last column : 
		for (int i = 0 ; i < h-1 ; i++ ){
			int value = tileValues[i][w-1];
			if (value == 0 || tileValues[i+1][w-1] == value) return false;
		}
		return true;
	}

	/******* Get/Set *********/
	public void setScore(int score){
		this.score=score;
	}
	public int getScore(){
		return score;
	}
	public int getH() {
		return h;
	}
	public int getW() {
		return w;
	}
	public int[][] getTiles(){
		return tileValues;
	}
}
