package core.game.adt;

public interface IGrid<Action,State> {

	/**
	 * Move in a direction
	 * @param a
	 * @param s
	 * @return true if moved
	 */
	public boolean move(Action a, State s);
	
	/**
	 * merge in a direction
	 * @param a
	 * @param s
	 * @return true if something moved/merged
	 */
	public boolean merge(Action a, State s);
	
}
