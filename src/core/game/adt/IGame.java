package core.game.adt;

public interface IGame<Action,State> {

	/**
	 * Modifies the state s according to the action a
	 * @param a
	 * @param s
	 * @return true if the action was possible
	 */
	public boolean playerAction(Action a, State s);
	
	/**
	 * Nature's response, 
	 * @param s is modified
	 * @return false is the game is over
	 */
	public boolean natureAction(State s);
}
