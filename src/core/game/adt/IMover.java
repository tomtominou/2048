package core.game.adt;

public interface IMover<State> {

	public boolean move(State s);
	public boolean move(int rowOrCol, State s);
	public boolean moveFrom(int i, int j, State s);	
	
}
