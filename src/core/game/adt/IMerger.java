package core.game.adt;

public interface IMerger<State> {

	boolean merge(State s);

}
