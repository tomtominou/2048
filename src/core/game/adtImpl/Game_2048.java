package core.game.adtImpl;

import java.util.List;

import core.game.adt.IGame;
import core.game.adtImpl.grid.Grid;
import core.state.GameState;

import util.Direction;

public class Game_2048 implements IGame<Direction, GameState> {

	private Grid grid = new Grid();
	public static double P2 = .9;
	
	@Override
	public boolean playerAction(Direction a, GameState s) {
		// and move the lines/columns as long as possible, and then merge :
		boolean moved = grid.move(a, s);
		boolean merged = grid.merge(a, s);
		// Return the resulting state :
		return merged || moved ;
	}

	@Override
	public boolean natureAction(GameState s) {
		// Get the list of zeros :
		List<Integer> zerosIndexes = s.getZerosIndexes();
		// If this list is empty, then game over
		if (zerosIndexes.size() == 0) return false;
		// pick a random index :
		int index = (int) (Math.random()*(double)zerosIndexes.size());
		// And a random value :
		int value = Math.random() > P2 ? 4 : 2 ;
		s.setTileValue(zerosIndexes.get(index), value);
		return true;
	}

}
