package core.game.adtImpl.mergers;

import core.game.adt.IMerger;
import core.game.adt.IMover;
import core.state.GameState;

public class DownMerger implements IMerger<GameState> {

	private IMover<GameState> mover;

	public DownMerger(IMover<GameState> mover){
		this.mover = mover;
	}

	@Override
	public boolean merge(GameState s) {
		boolean output = false;
		for (int col = 0 ; col < s.getW() ; col ++)
			if (merge(col,s)) 
				output = true;
		return output;
	}

	private boolean merge(int col, GameState s){
		boolean merged = false;
		for (int row = s.getH()-1 ; row > 0 ; row--){
			int value = s.getTileValue(row, col);
			if (value !=0 && value == s.getTileValue(row-1, col)) {
				s.setTileValue(row, col, 2*value);
				s.setTileValue(row-1, col, 0);
				mover.moveFrom(row-1, col, s);
				s.incrementScore(value);
				merged = true;
			}
		}
		return merged;
	}

}
