package core.game.adtImpl.mergers;

import core.game.adt.IMerger;
import core.game.adt.IMover;
import core.state.GameState;

public class RightMerger implements IMerger<GameState>{

	private IMover<GameState> mover;
	
	public RightMerger(IMover<GameState> mover){
		this.mover = mover;
	}
	
	@Override
	public boolean merge(GameState s) {
		boolean output = false;
		for (int row = 0 ; row < s.getH() ; row ++)
			if (merge(row,s)) 
				output = true;
		return output;
	}
	
	private boolean merge(int row, GameState s){
		boolean merged = false;
		for (int col = s.getW()-1 ; col > 0 ; col--){
			int value = s.getTileValue(row, col);
			if (value !=0 && value == s.getTileValue(row, col-1)) {
				s.setTileValue(row, col, 2*value);
				s.setTileValue(row, col-1, 0);
				mover.moveFrom(row, col-1, s);
				s.incrementScore(value);
				merged = true;
			}
		}
		return merged;
	}

}
