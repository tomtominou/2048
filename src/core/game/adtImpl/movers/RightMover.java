package core.game.adtImpl.movers;

import core.game.adt.IMover;
import core.state.GameState;

public class RightMover implements IMover<GameState>{

	@Override
	public boolean moveFrom(int row, int col, GameState s) {
		int moveOf = 0;
		boolean moved = false;
		for (int j = col ; j >= 0 ; j--){
			if (s.getTileValue(row, j) == 0) moveOf++;
			else
				if (moveOf != 0){
					s.setTileValue(row, j+moveOf, s.getTileValue(row, j));
					s.setTileValue(row, j, 0);
					moved = true;
				}
		}
		return moved;
	}

	@Override
	public boolean move(int row, GameState s) {
		return moveFrom(row, s.getW()-1, s);
	}
	
	@Override
	public boolean move(GameState s) {
		boolean moved = false;
		for (int row = 0 ; row < s.getH() ; row ++) 
			if(move(row,s)) 
				moved = true;
		return moved;
	}
}
