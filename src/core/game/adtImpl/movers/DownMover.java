package core.game.adtImpl.movers;

import core.game.adt.IMover;
import core.state.GameState;

public class DownMover implements IMover<GameState> {

	@Override
	public boolean moveFrom(int row, int col, GameState s) {
		int moveOf = 0;
		boolean moved = false;
		for (int i = row ; i >= 0 ; i--){
			if (s.getTileValue(i, col) == 0) moveOf++;
			else
				if (moveOf != 0){
					s.setTileValue(i+moveOf, col, s.getTileValue(i, col));
					s.setTileValue(i, col, 0);
					moved = true;
				}
		}
		return moved;
	}

	@Override
	public boolean move(int col, GameState s) {
		return moveFrom(s.getH()-1, col, s);
	}
	
	@Override
	public boolean move(GameState s) {
		boolean moved = false;
		for (int col = 0 ; col < s.getW() ; col ++) if(move(col,s)) moved = true;
		return moved;
	}

}
