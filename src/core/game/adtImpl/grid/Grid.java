package core.game.adtImpl.grid;

import java.util.HashMap;

import core.game.adt.IGrid;
import core.game.adt.IMerger;
import core.game.adt.IMover;
import core.game.adtImpl.mergers.DownMerger;
import core.game.adtImpl.mergers.LeftMerger;
import core.game.adtImpl.mergers.RightMerger;
import core.game.adtImpl.mergers.UpMerger;
import core.game.adtImpl.movers.DownMover;
import core.game.adtImpl.movers.LeftMover;
import core.game.adtImpl.movers.RightMover;
import core.game.adtImpl.movers.UpMover;
import core.state.GameState;

import util.Direction;

public class Grid implements IGrid<Direction, GameState>{

	private HashMap<Direction, IMover<GameState>> movers = new HashMap<Direction, IMover<GameState>>();
	private HashMap<Direction, IMerger<GameState>> mergers = new HashMap<Direction, IMerger<GameState>>();
	
	public Grid(){
		IMover<GameState> right = new RightMover() ;
		movers.put(Direction.RIGHT,right);
		mergers.put(Direction.RIGHT, new RightMerger(right));
		IMover<GameState> left = new LeftMover() ;
		movers.put(Direction.LEFT,left);
		mergers.put(Direction.LEFT, new LeftMerger(left));
		IMover<GameState> up = new UpMover() ;
		movers.put(Direction.UP,up);
		mergers.put(Direction.UP, new UpMerger(up));
		IMover<GameState> down = new DownMover() ;
		movers.put(Direction.DOWN,down);
		mergers.put(Direction.DOWN, new DownMerger(down));
	}
	
	@Override
	public boolean move(Direction a, GameState s) {
		return movers.get(a).move(s);
	}


	@Override
	public boolean merge(Direction a, GameState s) {
		return mergers.get(a).merge(s);
	}

}
