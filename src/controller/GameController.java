package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import manager.PlayerGameManager;

import core.game.adt.IGame;
import core.state.GameState;
import util.Direction;
import view.PlayerView;

public class GameController<Game extends IGame<Direction, GameState>> implements KeyListener,ActionListener{

	private PlayerView<Game> view;
	private PlayerGameManager<Direction, Game> manager;
	protected boolean gameOver = false;
	
	public GameController (PlayerGameManager<Direction, Game> manager,PlayerView<Game> view){
		this.manager = manager;
		this.view = view;
		view.display(manager.getS());
	}
	
	public void IsGameOver(){
		if (gameOver) System.out.println(" ===== Game Over ====");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.getResetButton()){
			manager.reset();
			gameOver = false;
			view.display(manager.getS());
		}
		view.getFrame().requestFocus();
		IsGameOver();
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			gameOver = !manager.play(Direction.LEFT);
			break;
		case KeyEvent.VK_RIGHT:
			gameOver = !manager.play(Direction.RIGHT);
			break;
		case KeyEvent.VK_UP:
			gameOver = !manager.play(Direction.UP);
			break;
		case KeyEvent.VK_DOWN:
			gameOver = !manager.play(Direction.DOWN);
			break;
		default:
			break;
		}
		view.display(manager.getS());
		IsGameOver();
	}

	@Override
	public void keyReleased(KeyEvent e) {		
	}

	@Override
	public void keyTyped(KeyEvent e) {		
	}

}
