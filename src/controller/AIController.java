package controller;

import java.awt.event.ActionEvent;

import javax.swing.Timer;

import manager.AIGameManager;
import util.Direction;
import view.AIView;
import core.game.adt.IGame;
import core.state.GameState;

public class AIController<Game extends IGame<Direction,GameState>> extends GameController<Game>{

	private AIView<Game> view;
	private AIGameManager<Direction, Game> manager;
	protected Timer timer;
	protected boolean autorunning = false;
	
	public AIController(AIGameManager<Direction, Game> manager, AIView<Game> view) {
		super(manager, view);
		
		this.manager = manager;
		this.view = view;
		timer = new Timer(10, this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == view.getResetButton()){
			view.getAutorunButton().setText("autorun : start");
		}
		if (e.getSource() == view.getNextButton()){
			gameOver = !manager.playBestMove();
			view.display(manager.getS());
		}
		if (e.getSource() == view.getAutorunButton()){
			autorunning = !autorunning ;
			if (autorunning) {
				view.getAutorunButton().setText("autorun : stop");
				timer.start();
			}
			else {
				view.getAutorunButton().setText("autorun : start");
				timer.stop();
			}
		}
		if (e.getSource() == timer && autorunning){
			gameOver = !manager.playBestMove();
			view.display(manager.getS());
			if (!gameOver) timer.start();
			else timer.stop();
		}
		super.actionPerformed(e);
	}

}
