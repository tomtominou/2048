package controller;

import manager.DesignerGameManager;
import util.Direction;
import view.DesignerView;
import view.modifiableTile.TileModifiedEvent;
import view.modifiableTile.TileModifiedListener;
import core.game.adt.IGame;
import core.state.GameState;

public class DesignerController<Game extends IGame<Direction,GameState>> extends AIController<Game> implements TileModifiedListener{

	private DesignerView<Game> view;
	private DesignerGameManager<Direction, Game> manager;
	
	public DesignerController(DesignerGameManager<Direction, Game> manager,DesignerView<Game> view) {
		super(manager,view);

		this.view = view;
		this.manager = manager;
	}

	@Override
	public void tileModified(TileModifiedEvent e) {
		manager.forceTileValue(e.getIndex(), e.getValue());
		view.display(manager.getS());
		view.getFrame().requestFocus();
	}

}
